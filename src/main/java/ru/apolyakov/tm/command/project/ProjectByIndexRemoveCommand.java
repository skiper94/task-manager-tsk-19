package ru.apolyakov.tm.command.project;

import ru.apolyakov.tm.exception.entity.ProjectNotFoundException;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectByIndexRemoveCommand extends AbstractProjectCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    public String description() {
        return "Remove project by index";
    }

    @Override
    public void execute() {
        System.out.println("[DELETE PROJECT]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = serviceLocator.getProjectService().removeProjectByIndex(index, serviceLocator.getAuthService().getUserId());
        if (project == null) throw new ProjectNotFoundException();
    }

}
