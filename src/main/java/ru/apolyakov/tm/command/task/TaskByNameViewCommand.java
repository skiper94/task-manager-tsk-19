package ru.apolyakov.tm.command.task;

import ru.apolyakov.tm.exception.entity.TaskNotFoundException;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.util.TerminalUtil;

public final class TaskByNameViewCommand extends AbstractTaskCommand{

    @Override
    public String arg() {
        return "task-view-by-name";
    }

    @Override
    public String name() {
        return null;
    }

    @Override
    public String description() {
        return "View task by name";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneByName(name, serviceLocator.getAuthService().getUserId());
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }
}
