package ru.apolyakov.tm.command.task;

import ru.apolyakov.tm.exception.entity.TaskNotFoundException;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.util.TerminalUtil;

public final class TaskByNameFinishCommand extends AbstractTaskCommand{
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Override
    public String description() {
        return "Finish task by name";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishTaskByName(name, serviceLocator.getAuthService().getUserId());
        if (task == null) throw new TaskNotFoundException();
    }
}
