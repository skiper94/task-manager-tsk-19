package ru.apolyakov.tm.command.task;

import ru.apolyakov.tm.exception.empty.EmptyNameException;
import ru.apolyakov.tm.util.TerminalUtil;

public final class TaskByNameUpdateCommand extends AbstractTaskCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-by-name";
    }

    @Override
    public String description() {
        return "Update task by name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        if (serviceLocator.getTaskService().findOneByName(name, serviceLocator.getAuthService().getUserId()) == null) throw new EmptyNameException();
        System.out.println("ENTER NEW NAME:");
        final String nameNew = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        serviceLocator.getTaskService().updateTaskByName(name, nameNew, TerminalUtil.nextLine(), serviceLocator.getAuthService().getUserId());
    }
}
