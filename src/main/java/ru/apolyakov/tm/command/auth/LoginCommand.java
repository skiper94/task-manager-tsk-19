package ru.apolyakov.tm.command.auth;

import ru.apolyakov.tm.util.TerminalUtil;

public final class LoginCommand extends AbstractAuthCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String description() {
        return "Login to the application";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }
}
