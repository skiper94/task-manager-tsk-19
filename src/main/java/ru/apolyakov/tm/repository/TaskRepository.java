package ru.apolyakov.tm.repository;

import ru.apolyakov.tm.api.repository.ITaskRepository;
import ru.apolyakov.tm.model.AbstractEntity;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task findOneByIndex(final Integer index, final String userId){
        List<Task> tasks = findAll(userId);
        return tasks.get(index);
    }

    @Override
    public Task findOneByName(final String name, final String userId){
        for (final Task task: entities){
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final Integer index, final String userId){
        final Task task = findOneByIndex(index, userId);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name, final String userId){
        final Task task = findOneByName(name, userId);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId, final String userId) {
        List<Task> listOfTask = new ArrayList<>();
        for (Task task : entities) {
            if (!userId.equals(task.getUserId())) continue;
            if (projectId.equals(task.getProjectId())) listOfTask.add(task);
        }
        return listOfTask;
    }

    @Override
    public List<Task> removeAllTaskByProjectId(final String projectId, final String userId) {
        final List<Task> listOfTask = findAllTaskByProjectId(projectId, userId);
        if (listOfTask == null) return null;
        for (Task task: listOfTask)
            entities.remove(task);
        return entities;
    }

    @Override
    public Task bindTaskByProject(final String projectId, final String taskId, final String userId) {
        final Task task = findOneById(taskId, userId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskByProjectId(final String taskId, final String userId) {
        final Task task = findOneById(taskId, userId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
