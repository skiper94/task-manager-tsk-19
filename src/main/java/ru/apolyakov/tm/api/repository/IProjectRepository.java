package ru.apolyakov.tm.api.repository;

import ru.apolyakov.tm.api.IRepository;
import ru.apolyakov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project findOneByIndex(Integer index, String userId);

    Project findOneByName(String name, String userId);

    Project removeOneByIndex(Integer index, String userId);

    Project removeOneByName(String name, String userId);

}
