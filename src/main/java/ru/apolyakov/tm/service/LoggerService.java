package ru.apolyakov.tm.service;

import ru.apolyakov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

public class LoggerService implements ILoggerService {

    private static String COMMANDS = "COMMANDS";
    private static String COMMANDS_FILE = ".commands.txt";

    private static String ERRORS = "ERRORS";
    private static String ERRORS_FILE = ".errors.txt";

    private static String MESSAGES = "MESSAGES";
    private static String MESSAGES_FILE = ".messages.txt";

    private final Logger root = Logger.getLogger("");
    private final Logger commands = Logger.getLogger(COMMANDS);
    private final Logger errors = Logger.getLogger(ERRORS);
    private final Logger messages = Logger.getLogger(MESSAGES);

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });

        return consoleHandler;
    }

    {
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERRORS_FILE, true);
        registry(messages, MESSAGES_FILE, true);
    }

    private void registry(
            final Logger logger, final String filename, final boolean isConsole
    ){
        try{
            logger.setUseParentHandlers(false);
            if (isConsole) logger.addHandler(getConsoleHandler());
            logger.addHandler(new FileHandler(filename));
        } catch (final IOException e) {
           root.severe(e.getMessage());
        }

    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }
}
