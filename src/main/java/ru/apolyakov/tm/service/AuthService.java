package ru.apolyakov.tm.service;

import ru.apolyakov.tm.api.service.IAuthService;
import ru.apolyakov.tm.api.service.IUserService;
import ru.apolyakov.tm.exception.user.AccessDeniedException;
import ru.apolyakov.tm.exception.user.EmptyLoginException;
import ru.apolyakov.tm.exception.user.EmptyPasswordException;
import ru.apolyakov.tm.model.User;
import ru.apolyakov.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.add(login, password, email);
    }

}
