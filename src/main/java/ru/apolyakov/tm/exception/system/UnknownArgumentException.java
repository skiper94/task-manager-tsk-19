package ru.apolyakov.tm.exception.system;

import ru.apolyakov.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Incorrect argument");
    }

}
