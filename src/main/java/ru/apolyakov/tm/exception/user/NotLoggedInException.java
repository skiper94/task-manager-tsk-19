package ru.apolyakov.tm.exception.user;

import ru.apolyakov.tm.exception.AbstractException;

public class NotLoggedInException extends AbstractException {

    public NotLoggedInException() {
        super("Error! You are not logged in...");
    }

}
