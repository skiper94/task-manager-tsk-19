package ru.apolyakov.tm.exception.entity;

import ru.apolyakov.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found!");
    }

}
