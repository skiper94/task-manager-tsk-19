package ru.apolyakov.tm.util;

public interface CheckUtil {

    static boolean isEmpty(final String value){
        return value == null || value.isEmpty();
    }

}
